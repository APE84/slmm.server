﻿using Slmm.Firmware.Geo;
using Slmm.Logger;
using System;
using System.Threading;

namespace Slmm.Firmware
{
    public class SmartMower : MowerBase
    {
        private Coordinates CurrentPosition;

        private const int MowingSpeed = 1000;

        private static object MoveLock = new object();

        #region Constructor
        public SmartMower(ILog logger) : base(logger)
        {
            CurrentPosition = new Coordinates(0, 0);
        }

        public SmartMower(int x, int y, ILog logger) : base(logger)
        {
            CurrentPosition = new Coordinates(x, y);
        }

        public SmartMower(Coordinates position, ILog logger) : base(logger)
        {
            CurrentPosition = position;
        }
        #endregion

        public override Coordinates GetPosition()
        {
            return CurrentPosition;
        }

        public override bool Move(Direction direction)
        {
            lock (MoveLock)
            {
                var newPosition = TranslateDiretionToCoordinates(CurrentPosition, direction);
                if (newPosition.y < 0 || newPosition.x < 0)
                {
                    Logger.Info($"Can't Move {Enum.GetName(typeof(Direction), direction)} - {newPosition}");
                    return false;
                }
                Logger.Info($"Start Move {Enum.GetName(typeof(Direction), direction)} - {newPosition}");
                Thread.Sleep(MowingSpeed);

                CurrentPosition = newPosition;
                Logger.Info($"End Move {Enum.GetName(typeof(Direction), direction)} - {newPosition}");
                return true;
            }
        }

        /// <summary>
        /// It's supposed to be always mowing
        /// </summary>
        /// <returns></returns>
        public override bool Mow()
        {
            return true;
        }
    }
}