﻿namespace Slmm.Firmware.Geo
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}