﻿using Slmm.Firmware.Environment;
using Slmm.Firmware.Geo;
using Slmm.Logger;
using System;

namespace Slmm.Firmware
{
    public class GardenManager
    {
        #region Properties
        public readonly ILawn Lawn;

        public readonly IMower Mower;

        private static GardenManager _instance;

        private static bool _instanceInitialized = false;
        #endregion

        #region Constructor
        public GardenManager(ILawn lawn, IMower mower)
        {
            if (lawn == null)
            {
                throw new ArgumentNullException(nameof(lawn));
            }
            if (mower == null)
            {
                throw new ArgumentNullException(nameof(mower));
            }

            if (_instanceInitialized)
            {
                throw new InvalidOperationException("Instance already initialized, please reuse it, or rely on dependency injection to mainain only one instance.");
            }
            _instanceInitialized = true;

            Lawn = lawn;
            Mower = mower;
        }
        #endregion

        #region Static Accessors
        public static GardenManager Deploy(int width, int height, int x, int y, ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _instanceInitialized = false;
            var lawn = new StandardLawn(width, height);
            var mown = new SmartMower(new Coordinates(x, y), logger);
            _instance = new GardenManager(lawn, mown);
            return GetManager();
        }

        public static GardenManager GetManager()
        {
            if (_instance == null)
            {
                throw new InvalidOperationException("Environment not setup, please Deploy first");
            }

            return _instance;
        }

        public static bool IsInitialized()
        {
            return _instanceInitialized;
        }

        /// <summary>
        /// Debug aid, do not rely on it
        /// </summary>
#if DEBUG
        public static void HardReset()
        {
            _instance = null;
            _instanceInitialized = false;
        }
#endif
        #endregion

        #region Public
        public Coordinates Mow(Direction direction)
        {
            MoveAndMow(direction);
            return Mower.GetPosition();
        }
        #endregion

        #region Private
        private void MoveAndMow(Direction direction)
        {
            var currentMowerPosition = Mower.GetPosition();
            var futureCoordinates = MowerBase.TranslateDiretionToCoordinates(currentMowerPosition, direction);
            if (Lawn.IsPositionEnclosed(futureCoordinates))
            {
                Mower.Move(direction);
            }
        }
        #endregion
    }
}