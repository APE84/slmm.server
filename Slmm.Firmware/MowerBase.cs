﻿using Slmm.Firmware.Geo;
using Slmm.Logger;
using System;

namespace Slmm.Firmware
{
    public abstract class MowerBase : IMower
    {
        protected ILog Logger;

        public MowerBase(ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            Logger = logger;
        }

        public abstract Coordinates GetPosition();

        public abstract bool Move(Direction direction);

        public abstract bool Mow();

        public static Coordinates TranslateDiretionToCoordinates(Coordinates position, Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return new Coordinates(position.x, position.y + 1);
                case Direction.Right:
                    return new Coordinates(position.x + 1, position.y);
                case Direction.Down:
                    return new Coordinates(position.x, position.y - 1);
                case Direction.Left:
                    return new Coordinates(position.x - 1, position.y);
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction));
            }
        }
    }
}
