﻿using Slmm.Firmware.Geo;
using System;

namespace Slmm.Firmware.Environment
{
    public interface ILawn
    {
        bool IsPositionEnclosed(Coordinates position);

        Coordinates GetMaxCoordinates();

        Tuple<int, int> GetSize();

        void GetSize(out int width, out int height);
    }
}
