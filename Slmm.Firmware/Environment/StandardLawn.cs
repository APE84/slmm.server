﻿using Slmm.Firmware.Geo;
using System;

namespace Slmm.Firmware.Environment
{
    public class StandardLawn : ILawn
    {
        private readonly int Width;
        private readonly int Height;

        public StandardLawn(int width, int height)
        {
            if (width <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(width), "Value can't be zero or less");
            }
            if (height <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(height), "Value can't be zero or less");
            }

            Width = width;
            Height = height;
        }

        public bool IsPositionEnclosed(Coordinates position)
        {
            return IsCoordinateValid(position.x, Width) && IsCoordinateValid(position.y, Height);
        }

        public Coordinates GetMaxCoordinates()
        {
            return new Coordinates(Width - 1, Height - 1);
        }

        public Tuple<int, int> GetSize()
        {
            return Tuple.Create<int, int>(Width, Height);
        }

        public void GetSize(out int width, out int height)
        {
            width = Width;
            height = Height;
        }

        private bool IsCoordinateValid(int x, int max)
        {
            return x >= 0 && x < max;
        }
    }
}