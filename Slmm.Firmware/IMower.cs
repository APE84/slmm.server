﻿using Slmm.Firmware.Geo;

namespace Slmm.Firmware
{
    public interface IMower
    {
        Coordinates GetPosition();

        bool Move(Direction direction);

        bool Mow();
    }
}
