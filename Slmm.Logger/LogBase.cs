﻿using System;

namespace Slmm.Logger
{
    public abstract class LogBase : ILog
    {
        public abstract void Info(string message);

        protected string Format(DateTime date, string message)
        {
            return $"{date.ToString("HH:mm:ss")} - {message + Environment.NewLine}";
        }
    }
}
