﻿using System;
using System.Diagnostics;

namespace Slmm.Logger
{
    public class ConsoleLogger : LogBase
    {
        public ConsoleLogger()
        {
            Info("Logger Initialized!");
        }

        public override void Info(string message)
        {
            Debug.WriteLine(Format(DateTime.Now, message));
        }
    }
}
