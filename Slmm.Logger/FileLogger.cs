﻿using System;
using System.IO;

namespace Slmm.Logger
{
    public class FileLogger : LogBase
    {
        private string Path;

        public FileLogger(string pathToFile)
        {
            Path = pathToFile;
            Info("Logger Initialized!");
        }

        public override void Info(string message)
        {
            File.AppendAllText(Path, Format(DateTime.Now, message));
        }
    }
}
