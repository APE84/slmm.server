﻿namespace Slmm.Logger
{
    public interface ILog
    {
        void Info(string message);
    }
}
