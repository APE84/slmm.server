﻿using Slmm.Api.Filters;
using System.Web.Http;

namespace Slmm.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            UnityConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            var logActionsFilter = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(LogActionsAttribute));
            GlobalConfiguration.Configuration.Filters.Add(logActionsFilter as LogActionsAttribute);
        }
    }
}
