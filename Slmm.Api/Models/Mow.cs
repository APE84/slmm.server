﻿using Slmm.Firmware.Geo;

namespace Slmm.Api.Models
{
    public class Mow
    {
        public Direction Move { get; set; }
    }
}