﻿namespace Slmm.Api.Models
{
    public class Garden
    {
        public int LawnWidth { get; set; }
        public int LawnHeight { get; set; }
        public int MowerX { get; set; }
        public int MowerY { get; set; }
    }
}