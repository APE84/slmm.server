﻿using Slmm.Logger;
using System;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Slmm.Api.Filters
{
    public class LogActionsAttribute : ActionFilterAttribute
    {
        private static ILog Logger;

        public LogActionsAttribute(ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            if (Logger == null)
            {
                Logger = logger;
            }
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            Logger.Info($"Start {actionContext.Request.Method} '{actionContext.Request.RequestUri}'");
            base.OnActionExecuting(actionContext);
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            Logger.Info($"Finished {actionExecutedContext.Request.Method} '{actionExecutedContext.Request.RequestUri}'");
        }
    }
}