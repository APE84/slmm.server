using Microsoft.Practices.Unity;
using Slmm.Logger;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Http;
using Unity.WebApi;

namespace Slmm.Api
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

#if DEBUG
            RegisterConsoleLogger(container);
#else
            RegisterFileLogger(container);
#endif

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }

        private static void RegisterConsoleLogger(UnityContainer container)
        {
            container.RegisterType<ILog, ConsoleLogger>(new ContainerControlledLifetimeManager());
        }

        private static void RegisterFileLogger(UnityContainer container)
        {
            var logFilePath = ConfigurationManager.AppSettings["Logger.File.Path"];
            if (!Path.IsPathRooted(logFilePath))
            {
                logFilePath = Path.Combine(HttpRuntime.AppDomainAppPath, logFilePath);
            }
            container.RegisterType<ILog, FileLogger>(new ContainerControlledLifetimeManager(), new InjectionConstructor(Path.GetFullPath(logFilePath)));
        }
    }
}