﻿using Slmm.Api.Models;
using Slmm.Firmware;
using Slmm.Firmware.Geo;
using Slmm.Logger;
using System;
using System.Web.Http;

namespace Slmm.Api.Controllers
{
    [RoutePrefix("api")]
    public class LawnController : ApiController
    {
        private ILog Logger;

        public LawnController(ILog logger)
        {
            Logger = logger;
        }

        [HttpGet]
        [Route("lawn")]
        public IHttpActionResult PingStatus()
        {
            if (!GardenManager.IsInitialized())
            {
                return BadRequest("Mower not deployed, use POST /api/Lawn");
            }

            var manager = GardenManager.GetManager();
            var mowerPosition = manager.Mower.GetPosition();
            int lawnWidth, lawnHeight;
            manager.Lawn.GetSize(out lawnWidth, out lawnHeight);

            return Ok(new Garden()
            {
                LawnWidth = lawnWidth,
                LawnHeight = lawnHeight,
                MowerX = mowerPosition.x,
                MowerY = mowerPosition.y
            });
        }

        [HttpPost]
        [Route("lawn")]
        public IHttpActionResult DeployMower([FromBody]Garden garden)
        {
            if (garden == null)
            {
                throw new ArgumentNullException(nameof(garden));
            }

            GardenManager.Deploy(garden.LawnWidth, garden.LawnHeight, garden.MowerX, garden.MowerY, Logger);
            return Ok();
        }

        [HttpGet]
        [Route("lawn/mow")]
        public IHttpActionResult ListDirections([FromBody]Mow mow)
        {
            return Ok(new {
                Up = Direction.Up,
                Right = Direction.Right,
                Down = Direction.Down,
                Left = Direction.Left
            });
        }

        [HttpPost]
        [Route("lawn/mow")]
        public IHttpActionResult MoveMower([FromBody]Mow mow)
        {
            if (!GardenManager.IsInitialized())
            {
                return BadRequest("Mower not deployed, use POST /api/Lawn");
            }
            if (mow == null)
            {
                throw new ArgumentNullException(nameof(mow));
            }

            var manager = GardenManager.GetManager();
            var mowerPosition = GardenManager.GetManager().Mow(mow.Move);
            int lawnWidth, lawnHeight;
            manager.Lawn.GetSize(out lawnWidth, out lawnHeight);

            return Ok(new Garden()
            {
                LawnWidth = lawnWidth,
                LawnHeight = lawnHeight,
                MowerX = mowerPosition.x,
                MowerY = mowerPosition.y
            });
        }
    }
}
