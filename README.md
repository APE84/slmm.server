SLMM - Instructions
===================

This is the SLMM Server, it exposes the mower API thru a Restful interface.

The solution is built with C# 6, and it supports Visual Studio 2015, which is required to build, and debug it.

It also depends on several NuGet packages.

Unit Testing covers the core functionalities, but not the Restful APIs.

Logging is configurable, there are two interfaces available, debug console, and file.
As it is the console logger is setup for the debug environment, and file one for the release configurations, it is possible to always use file logger by changing the dependency in App_Start/UnityConfig.cs.
The file logger requires a configuration to specify on which file to log on, this is found in the Web.config file, under "appSettings/Logger.File.Path".

The address on which the solution is deployed depends on the environment in which is deployed, but for the debug run in VS it is configurable in the settings of the "Slmm.Api" project, which is already setup as startup project.


SLMM - Original specifications
=====================================================

You are encouraged to take no more than 5 hours to implement this, but you are free to take less or more if you want to. Please do not overengineer this exercise, but you are welcome to demonstrate knowledge in a field that can influence this project.

2 – Exercise for senior server-side positions
-------------------------------------
Assume we are interested in creating a smart lawn mowing machine called SLMM (smart lawn mowing machine). Your task is to create the software that will run inside the SLMM itself as a server and a client that will be used by human operators to mow the lawn. This version of SLMM starts mowing immediately when deployed, and never stops. You also don't need to rotate it at all, and it only supports the following commands:

1. Move one position up
1. Move one positions down
1. Move one position to the left
1. Move one position to the right
1. Set start location and lawn size (width and height)

All move actions take one second to perform. Please emulate this using Thread.Sleep or Task.Wait in your code.

### Deliverables

1. One application that represents the server application exposed through a Restfull API (you are free to use any web framework of your choice) which emulates the work of SLMM. This should contain any and all logic encompassing the operation (and validations) of the SLMM. This would allow multiple clients to control the SLMM in parallel.
1. One console application that accepts command input from console and communicates it to the SLMM server application.
1. Documentation about how to build, run and test the solution.
1. Any and all automated testing code that you wrote
1. As a senior engineer, you are expected to provide meaningful input to the design of our solutions. As such it is required that you will also provide a short document which explains any assumptions and\or decisions made. You are also highly encouraged to comment on anything you found interesting or important. **Please attach this in email and do not include it in the PR**

You should provide access to an online repository that hosts all code for all applications, libraries and test projects. The readme should have all necessary information on how to build, run and test the solution.

### Acceptance criteria
1. The SLMM logic resides in the server application
1. The SLMM never goes outside of the dimensions of the garden as supplied during startup
1. The SLMM behaves consistently
1. The SLMM should write some messages for debugging purposes for any and all input. This can be either console, a file (through logging for example or manually if this is easier for you) or Debug window in VS. Any one will do.
1. The format of the debugging messages required on above point should be: “{Time} – {Action to take\has taken place} – {current location of SLMM}”.
  For example: "12:05:12 - Start Move Left - (5,2)" and after the action finished "12:05:22 - End Move Left - (5,2)". You are free to deviate from this format, but explain your reasons.

Assessment criteria
===================
We do not want to disclose exactly how we evaluate, but decisions will be made based on the following:

1. Following the acceptance criteria
1. Structure of program
1. Testing and testing strategy
1. Maintainability
1. Following standards & practices
1. The documement with comments provided will be taken into _heavy_ consideration

**Please note**: If the program does not build with the instructions provided, or it does not run successfully, the application will be rejected immediately.

If there is anything unclear or if you have any queries, please create an issue on this repository for the team to address.