﻿using NUnit.Framework;
using Slmm.Firmware;
using Slmm.Firmware.Geo;
using Slmm.Logger;

namespace Slmm.Tests.Unit
{
    [TestFixture]
    public class SmartMowerTests
    {
        private SmartMower DefaultMower;

        [SetUp]
        public void SetUp()
        {
            DefaultMower = new SmartMower(new NullLogger());
        }

        [Test]
        public void GetPositionDefaultConstructor()
        {
            var expected = new Coordinates(0, 0);

            Assert.AreEqual(expected, DefaultMower.GetPosition());
        }

        [Test]
        public void GetPositionCustomConstructor()
        {
            var expected = new Coordinates(0, 0);
            var mower = new SmartMower(expected, new NullLogger());

            Assert.AreEqual(expected, mower.GetPosition());
        }

        [Test]
        [TestCase(Direction.Up, true, 0, 1)]
        [TestCase(Direction.Right, true, 1, 0)]
        [TestCase(Direction.Down, false, 0, 0)]
        [TestCase(Direction.Left, false, 0, 0)]
        public void MoveTopRight(Direction direction, bool haveMoved, int expectedNewX, int expectedNewY)
        {
            var expectedPostion = new Coordinates(expectedNewX, expectedNewY);

            Assert.AreEqual(haveMoved, DefaultMower.Move(direction));
            Assert.AreEqual(expectedPostion, DefaultMower.GetPosition());
        }

        [Test]
        [TestCase(Direction.Down, 10, 9)]
        [TestCase(Direction.Left, 9, 10)]
        public void MoveBottomLeft(Direction direction, int expectedNewX, int expectedNewY)
        {
            var mower = new SmartMower(new Coordinates(10, 10), new NullLogger());
            var expectedPostion = new Coordinates(expectedNewX, expectedNewY);

            Assert.IsTrue(mower.Move(direction));
            Assert.AreEqual(expectedPostion, mower.GetPosition());
        }

        [Test]
        public void Mow()
        {
            Assert.IsTrue(DefaultMower.Mow());
        }
    }
}
