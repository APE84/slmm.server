﻿using NUnit.Framework;
using Slmm.Firmware;
using Slmm.Firmware.Geo;

namespace Slmm.Tests.Unit
{
    [TestFixture]
    public class MowerBaseTests
    {
        [Test]
        [TestCase(Direction.Up, 0, 0, 0, 1)]
        [TestCase(Direction.Up, 0, 1, 0, 2)]
        [TestCase(Direction.Right, 0, 0, 1, 0)]
        [TestCase(Direction.Right, 1, 0, 2, 0)]
        [TestCase(Direction.Down, 0, 0, 0, -1)]
        [TestCase(Direction.Down, 0, 1, 0, 0)]
        [TestCase(Direction.Left, 0, 0, -1, 0)]
        [TestCase(Direction.Left, 1, 0, 0, 0)]
        public void TestGetPositionDefaultConstructor(Direction move, int x1, int y1, int x2, int y2)
        {
            var actual = MowerBase.TranslateDiretionToCoordinates(new Coordinates(x1, y1), move);
            Assert.AreEqual(x2, actual.x);
            Assert.AreEqual(y2, actual.y);
        }
    }
}
