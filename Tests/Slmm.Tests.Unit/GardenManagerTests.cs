﻿using NUnit.Framework;
using Slmm.Firmware;
using Slmm.Firmware.Environment;
using Slmm.Firmware.Geo;
using Slmm.Logger;
using System;

namespace Slmm.Tests.Unit
{
    [TestFixture]
    public class GardenManagerTests
    {
        private int DefaultDimenstion = 10;
        private ILog Logger = new NullLogger();

        [Test]
        public void NotASingleton()
        {
            GardenManager.HardReset();
            Assert.DoesNotThrow(() => new GardenManager(new StandardLawn(DefaultDimenstion, DefaultDimenstion), new SmartMower(new Coordinates(0, 0), Logger)));
        }

        [Test]
        public void ConstructorMultiple()
        {
            GardenManager.HardReset();
            GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, 0, 0, Logger);
            Assert.Throws<InvalidOperationException>(() => new GardenManager(new StandardLawn(DefaultDimenstion, DefaultDimenstion), new SmartMower(new Coordinates(0, 0), Logger)));
        }

        [Test]
        public void Deploy()
        {
            GardenManager.HardReset();
            var manager = GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, 0, 0, Logger);
            Assert.IsInstanceOf(typeof(GardenManager), manager);
        }

        [Test]
        public void DeployMultiple()
        {
            GardenManager.HardReset();
            GardenManager.Deploy(DefaultDimenstion /2, DefaultDimenstion /2, 0, 0, Logger);
            var manager = GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, DefaultDimenstion, DefaultDimenstion, Logger);
            Assert.AreEqual(DefaultDimenstion - 1, manager.Lawn.GetMaxCoordinates().x);
            Assert.AreEqual(DefaultDimenstion - 1, manager.Lawn.GetMaxCoordinates().y);
            Assert.AreEqual(DefaultDimenstion, manager.Mower.GetPosition().x);
            Assert.AreEqual(DefaultDimenstion, manager.Mower.GetPosition().y);

        }

        [Test]
        public void GetManagerUndeployed()
        {
            GardenManager.HardReset();
            Assert.IsFalse(GardenManager.IsInitialized(), "Please run me on my own");
            Assert.Throws<InvalidOperationException>(() => GardenManager.GetManager());
        }

        [Test]
        public void GetManager()
        {
            GardenManager.HardReset();
            GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, 0, 0, Logger);
            Assert.IsInstanceOf(typeof(GardenManager), GardenManager.GetManager());
        }

        [Test]
        public void Mow()
        {
            GardenManager.HardReset();
            var manager = GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, 0, 0, Logger);

            manager.Mow(Direction.Right);
            Assert.AreEqual(new Coordinates(1, 0), manager.Mower.GetPosition());
            manager.Mow(Direction.Right);
            manager.Mow(Direction.Right);
            manager.Mow(Direction.Up);
            Assert.AreEqual(new Coordinates(3, 1), manager.Mower.GetPosition());
            manager.Mow(Direction.Up);
            manager.Mow(Direction.Up);
            Assert.AreEqual(new Coordinates(3, 3), manager.Mower.GetPosition());
            manager.Mow(Direction.Down);
            manager.Mow(Direction.Down);
            manager.Mow(Direction.Down);
            manager.Mow(Direction.Left);
            manager.Mow(Direction.Left);
            manager.Mow(Direction.Left);
            Assert.AreEqual(new Coordinates(0, 0), manager.Mower.GetPosition());
        }

        [Test]
        public void TryToLeaveLawn()
        {
            GardenManager.HardReset();
            var manager = GardenManager.Deploy(DefaultDimenstion, DefaultDimenstion, 0, 0, Logger);

            manager.Mow(Direction.Left);
            manager.Mow(Direction.Left);
            manager.Mow(Direction.Left);
            manager.Mow(Direction.Down);
            manager.Mow(Direction.Down);
            manager.Mow(Direction.Down);
            Assert.AreEqual(new Coordinates(0, 0), manager.Mower.GetPosition());
        }
    }
}
