﻿using NUnit.Framework;
using Slmm.Firmware.Environment;
using Slmm.Firmware.Geo;
using System;

namespace Slmm.Tests.Unit
{
    [TestFixture]
    public class StandardLawnTests
    {
        private const int DefaultWidth = 10;

        private const int DefaultHeight = 10;

        private StandardLawn GardensLawn { get; set; }

        [SetUp]
        public void SetUp()
        {
            GardensLawn = new StandardLawn(DefaultWidth, DefaultHeight);
        }

        [Test]
        [TestCase(-1, -1)]
        [TestCase(-1, DefaultHeight)]
        [TestCase(DefaultWidth, -1)]
        [TestCase(0, 0)]
        [TestCase(0, DefaultHeight)]
        [TestCase(DefaultWidth, 0)]
        public void Constructor(int width, int height)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => { new StandardLawn(width, height); });
        }

        [Test]
        [TestCase(0, 0, true)]
        [TestCase(DefaultWidth-1, DefaultHeight-1, true)]
        [TestCase(-1, -1, false)]
        [TestCase(0, -1, false)]
        [TestCase(-1, 0, false)]
        [TestCase(DefaultWidth, DefaultHeight-1, false)]
        [TestCase(DefaultWidth-1, DefaultHeight, false)]
        [TestCase(DefaultWidth, DefaultHeight, false)]
        public void IsPositionEnclosed(int x, int y, bool expectedResult)
        {
            Assert.AreEqual(expectedResult, GardensLawn.IsPositionEnclosed(new Coordinates(x, y)));
        }

        [Test]
        public void GetMaxCoordinates()
        {
            var expected = new Coordinates(DefaultWidth - 1, DefaultHeight - 1);
            Assert.AreEqual(expected, GardensLawn.GetMaxCoordinates());
        }
    }
}
